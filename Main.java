import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("What is your name? ");
        String name = scanner.nextLine();
        System.out.println("How old are you? ");
        int age = scanner.nextInt();
        scanner.nextLine();
        System.out.println("What is your motivation to become a backend developer? ");
        String motivation = scanner.nextLine();

        System.out.println("Hi! My name is " + name + " and I am " + age + " years old");
        System.out.println("My motivation to become a backend developer is " + motivation);
        scanner.close();
    }
}
